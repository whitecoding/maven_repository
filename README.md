# Maven Repository

## Getting Started

To add repository:

### Gradle

```groovy
repositories {
    maven {
        url "https://api.bitbucket.org/1.0/repositories/whitecoding/maven_repository/raw/releases"
    }
}
```

### Maven

```xml
<project>
  <repositories>
    <repository>
      <id>whitecoding-repository</id>
      <name>Whitecoding Repository</name>
      <url>https://api.bitbucket.org/1.0/repositories/whitecoding/maven_repository/raw/releases</url>
    </repository>
  <repositories>
</project>
```
